package com.name.util;

import java.io.IOException;
import java.time.LocalTime;

public interface LogWriter {
	/**
	 * start the log
	 * 
	 * @throws IllegalStateException by {@link #startAble()}
	 */
	void start();

	/**
	 * start the log with msg
	 * 
	 * @throws IllegalStateException by {@link #startAble()}
	 */
	void start(String msg);

	/**
	 * start the log with default msg, "start: hh:mm:ss"
	 * 
	 * @throws IllegalStateException by {@link #startAble()}
	 */
	void startWithDefaultMsg(LocalTime time);

	/**
	 * add msg to the log
	 * 
	 * @throws IllegalStateException by {@link #addAble()}
	 */
	void add(String msg);

	/**
	 * finish the log
	 * 
	 * @throws IllegalStateException by {@link #finishAble()}
	 */
	void finish();

	/**
	 * append noContentMsg if {@link #add()} never calls during staring mode, append
	 * msg and then write and finish the log
	 * 
	 * @throws IllegalStateException by {@link #finishAble()}
	 */
	void finish(String noContentMsg, String msg);

	/**
	 * finish the log with default msg, "no content", "end: hh:mm:ss"
	 * 
	 * @throws IllegalStateException by {@link #finishAble()}
	 */
	void finishWithDefaultMsg();

	/**
	 * check weather the log can be start, true if the log is not yet in starting
	 * mode
	 */
	boolean startAble();

	/**
	 * check weather the log is ready to append any msg, true if the log is in
	 * starting mode
	 */
	boolean addAble();

	/**
	 * check weather the log can be finish, true if the log is in starting mode,
	 * logPath, logName are already set
	 */
	boolean finishAble();

	/** set path to the log */
	void setLogPath(String path) throws IOException;

	/** set name to the log */
	void setLogName(String name);

}
