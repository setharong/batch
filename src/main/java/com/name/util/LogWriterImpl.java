package com.name.util;

import java.time.LocalTime;

import org.springframework.stereotype.Component;

@Component
public class LogWriterImpl implements LogWriter {

	private String path;
	private String name;
	private StringBuffer sb;
	private boolean noContent;

	@Override
	public void start() {
		start(null);
	}

	@Override
	public void start(String msg) {
		if (!startAble()) {
			throw new IllegalStateException("unable to start the log");
		}
		sb = new StringBuffer();
		noContent = true;
		addLine(msg);
	}

	@Override
	public void startWithDefaultMsg(LocalTime time) {
		String msg = "start: " + DateUtils.getStringIsoTime();
		start(msg);
	}

	@Override
	public void add(String msg) {
		if (!addAble()) {
			throw new IllegalStateException("unable to add msg to the log");
		}
		boolean added = addLine(msg);
		if (noContent && added) {
			noContent = false;
		}
	}

	@Override
	public void finish() {
		finish(null, null);
	}

	@Override
	public void finish(String noContentMsg, String msg) {
		if (!finishAble()) {
			throw new IllegalStateException("unable to finish the log");
		}
		if (noContent) {
			addLine(noContentMsg);
		}
		addLine(msg);
		write();
		sb = null;
	}

	@Override
	public void finishWithDefaultMsg() {
		String noContentMsg = "no content";
		String msg = "end: " + DateUtils.getStringIsoTime();
		finish(noContentMsg, msg);
	}

	@Override
	public boolean startAble() {
		return sb == null;
	}

	@Override
	public boolean addAble() {
		return !startAble();
	}

	@Override
	public boolean finishAble() {
		return addAble() && path != null && name != null;
	}

	@Override
	public void setLogPath(String path) {
		// throw exception if the path is unwritable by any permission
		this.path = path;
	}

	@Override
	public void setLogName(String name) {
		this.name = name + ".txt";
	}

	private boolean addLine(String msg) {
		if (msg != null) {
			sb.append(msg).append("\n");
			return true;
		}
		return false;
	}

	private void write() {
		System.out.println("**********************");
		System.out.println("* write log to " + path + name);
		System.out.println(sb);
		System.out.println("**********************");
	}

}
