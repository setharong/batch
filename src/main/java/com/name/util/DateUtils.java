package com.name.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public abstract class DateUtils {

	/** getLocalTime by now */
	public static LocalTime getLocalTime() {
		return LocalTime.now();
	}

	/** getLocalDate by now */
	public static LocalDate getLocalDate() {
		return LocalDate.now();
	}

	/** getLocalDateTime by now */
	public static LocalDateTime getLocalDateTime() {
		return LocalDateTime.now();
	}

	/** get string by now as specified formatter */
	public static String getString(DateTimeFormatter f) {
		return getString(f, getLocalDateTime());
	}

	/** get string by specified date/time as specified formatter */
	public static String getString(DateTimeFormatter f, TemporalAccessor ta) {
		return f.format(ta);
	}

	/** get string by now as specified pattern */
	public static String getString(String pattern) {
		return getString(pattern, getLocalDateTime());
	}

	/** get string by specified date/time as specified pattern */
	public static String getString(String pattern, TemporalAccessor ta) {
		return getString(DateTimeFormatter.ofPattern(pattern), ta);
	}

	/** get string by now as BASIC_ISO_DATE, "yyyymmdd" */
	public static String getStringBasicIsoDate() {
		return getStringBasicIsoDate(getLocalDate());
	}

	/** get string by specified date as BASIC_ISO_DATE, "yyyymmdd" */
	public static String getStringBasicIsoDate(LocalDate d) {
		return getString(DateTimeFormatter.BASIC_ISO_DATE, d);
	}

	/** get string by now as ISO_TIME, "hh:mm:ss" */
	public static String getStringIsoTime() {
		return getStringIsoTime(getLocalTime());
	}

	/** get string by specified time as ISO_TIME, "hh:mm:ss" */
	public static String getStringIsoTime(LocalTime t) {
		return getString(DateTimeFormatter.ISO_TIME, t.withNano(0));
	}

}
