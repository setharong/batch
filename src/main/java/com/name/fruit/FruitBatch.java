package com.name.fruit;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.name.batch.Batch;
import com.name.batch.Verifiable;

@Component
public class FruitBatch implements Batch, Verifiable {

	@Autowired
	private List<String> fruit;
	@Autowired
	private FruitLogWriter logWriter;

	@Override
	public void executes(LocalDateTime startHour) throws Exception {
		System.out.println("executing FruitBatch");
		logWriter.start(startHour);
		for (String f : fruit) {
			System.out.println("processing " + f);
			logWriter.add(f);
		}
		logWriter.finish();
	}

	@Override
	public void verify() throws Exception {
		System.out.println("verifying FruitBatch");
		Assert.notNull(fruit, "missing data");
	}

}
