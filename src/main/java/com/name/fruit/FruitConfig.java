package com.name.fruit;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FruitConfig {

	@Bean
	public List<String> getFruits() {
		return Arrays.asList("apple", "banana", "orange");
	}

}
