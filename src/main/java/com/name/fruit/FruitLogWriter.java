package com.name.fruit;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.name.batch.Verifiable;
import com.name.util.DateUtils;
import com.name.util.LogWriter;

@Component
public class FruitLogWriter implements Verifiable {

	@Autowired
	private LogWriter logWriter;

	@Override
	public void verify() throws Exception {
		System.out.println("verifying FruitLogWriter");
		logWriter.setLogPath("D:/"); // set in verifying stage
	}

	public void start(LocalDateTime startHour) {
		logWriter.setLogName("fruit_" + DateUtils.getStringBasicIsoDate(startHour.toLocalDate()));
		logWriter.startWithDefaultMsg(startHour.toLocalTime());
	}

	public void add(Object fruit) {
		logWriter.add("logging " + fruit);
	}

	public void finish() {
		logWriter.finishWithDefaultMsg();
	}

}
