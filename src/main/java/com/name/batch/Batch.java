package com.name.batch;

import java.time.LocalDateTime;

/**
 * One bean of this interface will auto {@link #executes(startHour)} by
 * {@link MainBatchExecutor}, need to specify the bean name by argument if there
 * are more than one beans
 */
public interface Batch {

	void executes(LocalDateTime startHour) throws Exception;

}
