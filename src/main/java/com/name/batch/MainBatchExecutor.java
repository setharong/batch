package com.name.batch;

import java.time.LocalDateTime;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.AbstractApplicationContext;

import com.name.util.DateUtils;

/**
 * main executor of the batch, verify all {@link Verifiable} beans and then
 * executes the specified bean (by argument) of type {@link Batch} (no need to
 * specify if there's only one bean of this type)
 */
@Configuration
@ComponentScan(basePackages = { "com.name" })
public class MainBatchExecutor {

	static final Log LOGGER = LogFactory.getLog(MainBatchExecutor.class);

	public static void main(String[] args) {
		LocalDateTime startHour = DateUtils.getLocalDateTime();

		LOGGER.info("initiating..");
		try (AbstractApplicationContext context = new AnnotationConfigApplicationContext(MainBatchExecutor.class)) {

			Collection<Verifiable> verifiables = context.getBeansOfType(Verifiable.class).values();
			LOGGER.info("verifying..");
			for (Verifiable v : verifiables) {
				v.verify();
			}

			Batch batch;
			if (args.length > 0) {
				batch = context.getBean(args[0], Batch.class);
			} else {
				batch = context.getBean(Batch.class);
			}
			LOGGER.info("executing..");
			batch.executes(startHour);
			LOGGER.info("execution finished.");

		} catch (Exception e) {
			LOGGER.error(e);
		}

	}

}
