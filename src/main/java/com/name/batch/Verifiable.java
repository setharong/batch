package com.name.batch;

/**
 * Any beans implements this interface will auto {@link #verify()} by
 * {@link MainBatchExecutor}
 */
public interface Verifiable {

	void verify() throws Exception;

}
